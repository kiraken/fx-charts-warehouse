from django.apps import AppConfig


class FxchartsConfig(AppConfig):
    name = 'fxcharts'
